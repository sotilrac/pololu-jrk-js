#!/bin/bash
# Install jrk dependencies

sudo apt install build-essential git cmake libudev-dev qtbase5-dev -y

mkdir tmp_files && cd tmp_files

git clone https://github.com/pololu/libusbp -b v1-latest --depth 1 &&
cd libusbp &&
mkdir build &&
cd build &&
cmake .. &&
make &&
sudo make install &&
cd ../.. &&

git clone https://github.com/pololu/pololu-jrk-g2-software jrk --depth 1 &&
cd jrk &&
sudo cp udev-rules/99-pololu.rules /etc/udev/rules.d/ &&
mkdir build &&
cd build &&
cmake .. &&
make &&
sudo make install &&
cd ../.. &&

sudo sh -c 'echo /usr/local/lib > /etc/ld.so.conf.d/local.conf' &&
sudo ldconfig &&

cd ..
rm -rf tmp_files
